package co.com.proyectobase.screenplay.reto2.stepdefinitions;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.List;

import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.reto2.questions.LaRespuesta;
import co.com.proyectobase.screenplay.reto2.tasks.Abrir;
import co.com.proyectobase.screenplay.reto2.tasks.CrearEmpleado;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class AutomationStepDefinition {
	
	@Managed(driver ="chrome")
	private WebDriver hisBrowser;
	private Actor juan = Actor.named("juan");
	
	@Before
	public void configuracionInicial() {
		juan.can(BrowseTheWeb.with(hisBrowser));
	}
	
	@Given("^That Juan needs to create an employee in the OrangeHR$")
	public void thatJuanNeedsToCreateAnEmployeeInTheOrangeHR() throws Exception {
		juan.wasAbleTo(Abrir.AutomationDemo());
	}


	@When("^He realice the entry of the record in the aplication$")
	public void heRealiceTheEntryOfTheRecordInTheAplication(DataTable form) throws Exception {
		List<List<String>> data = form.raw();
		juan.attemptsTo(CrearEmpleado.EnelFormulario(data));
	}

	@Then("^He visualizes the new employee with name .( *) in the aplication$")
	public void heVisualizesTheNewEmployeeInTheAplication(String Respuesta) throws Exception {
		juan.should(seeThat(LaRespuesta.es(), equalTo(Respuesta)));
	}


}
