package co.com.proyectobase.screenplay.reto2.runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		features="src/test/resources/features/AutomationOrange.feature",
		tags= "@tag1",
		glue="co.com.proyectobase.screenplay.reto2.stepdefinitions",
		snippets=SnippetType.CAMELCASE		)
public class RunnerTags {

}
