#Author: your.email@your.domain.com
@tag
Feature: OrangeHrm Enterprise
  Juan needs to create an employee on the OrangeHrm

  @tag1
  Scenario: Create an employee in the OrangeHrm
    Given That Juan needs to create an employee in the OrangeHR
    When He realice the entry of the record in the aplication
    	|<firstname>|<middlename>	|<lastname>	|<employeeid>	|<locationlist>			 |<employeename>		|<datebirth>|<marriedstatus>|<gender>	|<nationality>|<driverlicense>|<licenseexpiry>|<nickname>	|<militaryservice>|<bloodgroup>	|<hobbie>	|
    	|Juan				|Pablo				|Montoya		|64959				|Canadian Regional HQ|Juan Pablo Montoya|1998-03-24	|Married				|Male			|Brazilian		|QZ45231234			|2018-09-24			|jpmontoya	|632017						|A						|Futbol		|
    Then He visualizes the new employee with name Juan Pablo Montoya in the aplicatio
  @tag2
  Scenario Outline: Title of your scenario outline
    Given I want to write a step with <name>
    When I check for the <value> in step
    Then I verify the <status> in step

    Examples: 
      | name  | value | status  |
      | name1 |     5 | success |
      | name2 |     7 | Fail    |
