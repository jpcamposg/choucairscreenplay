package co.com.proyectobase.screenplay.reto2.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://orangehrm-demo-6x.orangehrmlive.com/")
public class AutomationDemoPage extends PageObject {
	
	public static final Target LOGIN = Target.the("Login Administrador")
			.located(By.id("btnLogin"));
	public static final Target BUTTON_PIM = Target.the("Modulo de Informacion Personal")
			.located(By.xpath("//*[@id=\"menu_pim_viewPimModule\"]/a/span[2]"));
	public static final Target BUTTON_ADDEMPLOYEE = Target.the("Agregar Empleado")
			.located(By.xpath("//*[@id=\"menu_pim_addEmployee\"]/span[2]"));
}
