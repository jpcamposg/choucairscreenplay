package co.com.proyectobase.screenplay.reto2.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class AutomationFormularioPage {
	
	public static final Target FIRST_NAME = Target.the("Primer Nombre")
			.located(By.id("firstName"));
	public static final Target MIDDLE_NAME = Target.the("Segundo Nombre")
			.located(By.id("middleName"));
	public static final Target LAST_NAME = Target.the("Apellido")
			.located(By.id("lastName"));
	public static final Target EMPLOYEE_ID = Target.the("ID empleado")
			.located(By.id("employeeId"));
	public static final Target LOCATION = Target.the("Location employee")
			.located(By.xpath("//*[@id='location_inputfileddiv']/div/input"));
	public static final Target LOCATION_LIST = Target.the("Location employee")
			.located(By.id("location_inputfileddiv"));;
	public static final Target SAVE_EMPLOYEE = Target.the("Save")
			.located(By.id("systemUserSaveBtn"));
	public static final Target EMPLOYEE_NAME = Target.the("Filtro por nombre")
			.located(By.id("employee_name_quick_filter_employee_list_value"));	
	public static final Target ADD = Target.the("Seleccionar Usuario")
			.located(By.id("addEmployeeButton"));
	public static final Target DATE_BIRTH = Target.the("Ingresar fecha de nacimiento")
			.located(By.id("date_of_birth"));
	public static final Target SELECT_STATUS = Target.the("Estado civil Usuario")
			.located(By.id("marital_status_inputfileddiv"));
	public static final Target OPTION_MALE = Target.the("Seleccionar Usuario")
			.located(By.xpath("//*[@id=\"pimPersonalDetailsForm\"]/materializecss-decorator[3]/div/sf-decorator[3]/div/ul/li[1]/label"));
	public static final Target OPTION_FEMALE = Target.the("Seleccionar Usuario")
			.located(By.xpath("//*[@id=\"pimPersonalDetailsForm\"]/materializecss-decorator[3]/div/sf-decorator[3]/div/ul/li[2]/label"));
	public static final Target SELECT_NATIONALITY = Target.the("Nacionalidad del empleado")
	.located(By.id("nationality_inputfileddiv"));
	public static final Target DRIVER_LICENSE = Target.the("Licencia de conduccion")
	.located(By.id("driver_license"));	
	public static final Target LICENSE_EXPIRY = Target.the("Fecha de expiracion licencia")
	.located(By.id("license_expiry_date"));
	public static final Target NICK_NAME = Target.the("Nick name del usuario")
			.located(By.id("nickName"));
	public static final Target MILITARY_SERVICE = Target.the("Libreta Militar")
			.located(By.id("militaryService"));
	public static final Target SMOKER = Target.the("El usuario fuma SI/NO")
			.located(By.xpath("//*[@id=\"pimPersonalDetailsForm\"]/materializecss-decorator[5]/div/sf-decorator[3]/div/label"));
	public static final Target SAVE_PERSONAL_DETAILS = Target.the("Guardar detalles")
			.located(By.xpath("//*[@id=\"pimPersonalDetailsForm\"]/materializecss-decorator[8]/div/sf-decorator[2]/div/button"));
	public static final Target BLOOD_GROUP = Target.the("Tipo de sangre")
			.located(By.cssSelector("1_inputfileddiv"));	
	public static final Target HOBBIES = Target.the("Hobbies del empleado")
			.located(By.id("5"));
	public static final Target SAVE_IMPORTANT = Target.the("Guardar important")
			.located(By.xpath("//*[@id=\"content\"]/div[2]/ui-view/div[2]/div/custom-fields-panel/div/form/materializecss-decorator[3]/div/sf-decorator/div/button"));
	public static final Target EMPLOYEE_LIST = Target.the("Hobbies del empleado")
			.located(By.id("menu_pim_viewEmployeeList"));
	public static final Target RESULT = Target.the("Nombre del empleado en la lista")
			.located(By.xpath("//*[@id=\"employeeListTable\"]/tbody/tr[1]/td[3]"));
	
}
