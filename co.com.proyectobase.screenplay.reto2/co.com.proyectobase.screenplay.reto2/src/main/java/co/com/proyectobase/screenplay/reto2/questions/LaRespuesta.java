package co.com.proyectobase.screenplay.reto2.questions;

import co.com.proyectobase.screenplay.reto2.userinterface.AutomationFormularioPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class LaRespuesta implements Question<String>{

	
	public static LaRespuesta es() {
		return new LaRespuesta();
	}
	
	@Override
	public String answeredBy(Actor actor) {
		return Text.of(AutomationFormularioPage.RESULT).viewedBy(actor).asString();
	}

}
