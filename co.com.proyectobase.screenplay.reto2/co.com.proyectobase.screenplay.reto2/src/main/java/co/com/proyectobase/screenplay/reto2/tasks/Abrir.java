package co.com.proyectobase.screenplay.reto2.tasks;

import co.com.proyectobase.screenplay.reto2.userinterface.AutomationDemoPage;
import co.com.proyectobase.screenplay.reto2.userinterface.AutomationFormularioPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.waits.WaitUntil;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class Abrir implements Task{

	AutomationDemoPage automationDemoPage;
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Open.browserOn(automationDemoPage));
		actor.attemptsTo(Click.on(AutomationDemoPage.LOGIN));
		actor.attemptsTo(Click.on(AutomationDemoPage.BUTTON_PIM));
		actor.attemptsTo(Click.on(AutomationDemoPage.BUTTON_ADDEMPLOYEE), WaitUntil.the(AutomationFormularioPage.FIRST_NAME,
				isVisible()).forNoMoreThan(15).seconds());
	}

	public static Abrir AutomationDemo() {
		return Tasks.instrumented(Abrir.class);
	}
	
	
		

		
}