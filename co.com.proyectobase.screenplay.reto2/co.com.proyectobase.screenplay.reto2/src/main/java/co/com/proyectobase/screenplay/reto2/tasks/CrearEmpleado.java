package co.com.proyectobase.screenplay.reto2.tasks;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.reto2.interactions.Select;
import co.com.proyectobase.screenplay.reto2.userinterface.AutomationDemoPage;
import co.com.proyectobase.screenplay.reto2.userinterface.AutomationFormularioPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Hit;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.serenitybdd.screenplay.actions.selectactions.SelectByIndexFromBy;
import net.serenitybdd.screenplay.actions.selectactions.SelectByVisibleTextFromBy;
import net.serenitybdd.screenplay.waits.WaitUntil;
import net.thucydides.core.annotations.locators.WaitForWebElements;
import net.thucydides.core.webdriver.WebdriverAssertionError;

public class CrearEmpleado implements Task{

		private List<List<String>> form;

		public CrearEmpleado(List<List<String>> form) {
			super();
			this.form = form;
		}
		
		
		@Override
		public <T extends Actor> void performAs(T actor) {
								
				actor.attemptsTo(Enter.theValue(form.get(1).get(0).trim()).into(AutomationFormularioPage.FIRST_NAME));
				actor.attemptsTo(Enter.theValue(form.get(1).get(1).trim()).into(AutomationFormularioPage.MIDDLE_NAME));
				actor.attemptsTo(Enter.theValue(form.get(1).get(2).trim()).into(AutomationFormularioPage.LAST_NAME));
				actor.attemptsTo(Enter.theValue(form.get(1).get(3).trim()).into(AutomationFormularioPage.EMPLOYEE_ID));
				actor.attemptsTo(Click.on(AutomationFormularioPage.LOCATION));
				actor.attemptsTo(Select.ofTheList(AutomationFormularioPage.LOCATION_LIST, form.get(1).get(4).trim()));
				actor.attemptsTo(Click.on(AutomationFormularioPage.SAVE_EMPLOYEE), WaitUntil.the(AutomationFormularioPage.EMPLOYEE_NAME,
						isVisible()).forNoMoreThan(15).seconds());
				//actor.attemptsTo(Enter.theValue(form.get(1).get(5).trim()).into(AutomationFormularioPage.EMPLOYEE_NAME));
				//actor.attemptsTo(Hit.the(Keys.ENTER).into(AutomationFormularioPage.EMPLOYEE_NAME), WaitUntil.the(AutomationFormularioPage.IMPORTANT,
				//		isVisible()).forNoMoreThan(15).seconds()); 
				//actor.attemptsTo(Click.on(AutomationFormularioPage.IMPORTANT));
				actor.attemptsTo(Enter.theValue(form.get(1).get(6).trim()).into(AutomationFormularioPage.DATE_BIRTH));
				actor.attemptsTo(Click.on(AutomationFormularioPage.SELECT_STATUS));
				actor.attemptsTo(Select.ofTheList(AutomationFormularioPage.SELECT_STATUS, form.get(1).get(7).trim()));
				if(form.get(1).get(8).trim().equals("Male")) {
					actor.attemptsTo(Click.on(AutomationFormularioPage.OPTION_MALE));
				}else {
					actor.attemptsTo(Click.on(AutomationFormularioPage.OPTION_MALE));
				}
				actor.attemptsTo(Click.on(AutomationFormularioPage.SELECT_NATIONALITY));
				actor.attemptsTo(Select.ofTheList(AutomationFormularioPage.SELECT_NATIONALITY, form.get(1).get(9).trim()));
				actor.attemptsTo(Enter.theValue(form.get(1).get(10).trim()).into(AutomationFormularioPage.DRIVER_LICENSE));
				actor.attemptsTo(Enter.theValue(form.get(1).get(11).trim()).into(AutomationFormularioPage.LICENSE_EXPIRY));
				actor.attemptsTo(Enter.theValue(form.get(1).get(12).trim()).into(AutomationFormularioPage.NICK_NAME));
				actor.attemptsTo(Enter.theValue(form.get(1).get(13).trim()).into(AutomationFormularioPage.MILITARY_SERVICE));
				actor.attemptsTo(Click.on(AutomationFormularioPage.SMOKER));
				actor.attemptsTo(Click.on(AutomationFormularioPage.SAVE_PERSONAL_DETAILS), WaitUntil.the(AutomationFormularioPage.BLOOD_GROUP,
						isVisible()).forNoMoreThan(5).seconds());
				actor.attemptsTo(Scroll.to(AutomationFormularioPage.SAVE_IMPORTANT));
				actor.attemptsTo(Click.on(AutomationFormularioPage.BLOOD_GROUP));
				actor.attemptsTo(Select.ofTheList(AutomationFormularioPage.BLOOD_GROUP, form.get(1).get(14).trim()));
				actor.attemptsTo(Enter.theValue(form.get(1).get(15).trim()).into(AutomationFormularioPage.HOBBIES));
				actor.attemptsTo(Click.on(AutomationFormularioPage.SAVE_IMPORTANT), WaitUntil.the(AutomationFormularioPage.HOBBIES,
						isVisible()).forNoMoreThan(5).seconds());
				actor.attemptsTo(Scroll.to(AutomationFormularioPage.EMPLOYEE_LIST));
				actor.attemptsTo(Click.on(AutomationFormularioPage.EMPLOYEE_LIST));
				actor.attemptsTo(Enter.theValue(form.get(1).get(5).trim()).into(AutomationFormularioPage.EMPLOYEE_NAME));
				actor.attemptsTo(Hit.the(Keys.ENTER).into(AutomationFormularioPage.EMPLOYEE_NAME));
				try {
					Thread.sleep(4000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
		}
		
		public static CrearEmpleado EnelFormulario(List<List<String>> form) {
			return Tasks.instrumented(CrearEmpleado.class, form);
		}
		
		
}
