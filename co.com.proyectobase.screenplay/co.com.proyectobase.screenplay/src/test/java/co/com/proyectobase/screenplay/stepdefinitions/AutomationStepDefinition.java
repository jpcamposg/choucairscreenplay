package co.com.proyectobase.screenplay.stepdefinitions;

import org.openqa.selenium.WebDriver;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.List;

import co.com.proyectobase.screenplay.questions.LaRespuesta;
import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.Registra;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class AutomationStepDefinition {
	
	@Managed(driver ="chrome")
	private WebDriver hisBrowser;
	private Actor carlos = Actor.named("carlos");
	
	@Before
	public void configuracionInicial() {
		carlos.can(BrowseTheWeb.with(hisBrowser));
	}
	
	@Given("^Carlos want to access to the Web Automation Demo Site$")
	public void carlosWantToAccessToTheWebAutomationDemoSite() throws Exception {
		carlos.wasAbleTo(Abrir.AutomationDemo());
	}


	@When("^He realice the register on the page$")
	public void heRealiceTheRegisterOnThePage(DataTable form) throws Exception {
		List<List<String>> data = form.raw();		
		carlos.attemptsTo(Registra.EnelFormulario(data));
	}

	@Then("^He verifies that it is loaded on the screen with the text (.*)$")
	public void heVerifiesThatItIsLoadedOnTheScreenWithTheTextDoubleClickOnEditIconToEDITTheTableRow(String Respuesta) throws Exception {
		carlos.should(seeThat(LaRespuesta.es(), equalTo(Respuesta)));
	}


}
