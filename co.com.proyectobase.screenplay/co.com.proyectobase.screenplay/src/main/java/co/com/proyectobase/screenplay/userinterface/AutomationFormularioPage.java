package co.com.proyectobase.screenplay.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class AutomationFormularioPage{

	public static final Target FIRST_NAME = Target.the("Primer Nombre del Usuario")
			.located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[1]/div[1]/input"));
	public static final Target LAST_NAME = Target.the("Apellido del usuario")
			.located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[1]/div[2]/input"));
	public static final Target ADDRESS = Target.the("Direccion del Usuario")
			.located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[2]/div/textarea"));
	public static final Target EMAIL = Target.the("Correo del Usuario")
			.located(By.xpath("//*[@id=\"eid\"]/input"));
	public static final Target PHONE = Target.the("Telefono del usuario")
			.located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[4]/div/input"));
	public static final Target GENDER_MALE = Target.the("Genero Masculino")
			.located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[5]/div/label[1]/input"));
	public static final Target GENDER_FEMALE = Target.the("Genero Femenino")
			.located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[5]/div/label[2]/input"));
	public static final Target BUTTON_CRICKET = Target.the("Hobbie Cricket")
			.located(By.id("checkbox1"));
	public static final Target BUTTON_MOVIES = Target.the("Hobbie Movies")
			.located(By.id("checkbox2"));
	public static final Target BUTTON_HOCKEY = Target.the("Hobbie Hockey")
			.located(By.id("checkbox3"));
	public static final Target SELECT_LANGUAGES= Target.the("Seleccione Un Lenguaje")
			.located(By.id("msdd"));
	public static final Target SELECT_ENGLISH= Target.the("Lenguaje Ingles")
			.located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[7]/div/multi-select/div[2]/ul/li[8]/a"));
	public static final Target SELECT_SKILL= Target.the("Seleccione Lenguaje de programacion")
			.located(By.id("Skills"));
	public static final Target SELECT_COUNTRY= Target.the("Seleccion Pais")
			.located(By.id("countries"));
	public static final Target SELECT_SELECTCOUNTRY= Target.the("Seleccion un pais")
			.located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[10]/div/span/span[1]/span"));
	public static final Target SELECT_RESULTCOUNTRY= Target.the("Seleccione el pais")
			.located(By.xpath("/html/body/span/span/span[1]/input"));
	public static final Target SELECT_SUGGESTIONCOUNTRY= Target.the("Pais Filtrado")
			.located(By.id("select2-country-results"));
	public static final Target SELECT_YEAR= Target.the("Seleccion Año de nacimiento")
			.located(By.id("yearbox"));
	public static final Target SELECT_MONTH= Target.the("Seleccione mes de nacimiento")
			.located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[11]/div[2]/select"));
	public static final Target SELECT_DAY= Target.the("Seleccione dia de nacimiento")
			.located(By.id("daybox"));
	public static final Target PASSWORD= Target.the("Agregue contraseña")
			.located(By.id("firstpassword"));
	public static final Target CONFIRM_PASSWORD= Target.the("Verificar contraseña")
			.located(By.id("secondpassword"));
	public static final Target BUTTON_SUBMIT= Target.the("Registrarse")
			.located(By.id("submitbtn"));
	public static final Target BUTTON_REFRESH = Target.the("Limpiar el formulario")
			.located(By.id("Button1"));
	public static final Target RESULT = Target.the("Primer Nombre del Usuario")
			.located(By.xpath("/html/body/section/div[1]/div/div[2]/h4[1]"));
	
	
	
}
