package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import co.com.proyectobase.screenplay.userinterface.AutomationDemoPage;
import co.com.proyectobase.screenplay.userinterface.AutomationFormularioPage;
import cucumber.api.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Hit;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.serenitybdd.screenplay.actions.selectactions.SelectByIndexFromBy;
import net.serenitybdd.screenplay.actions.selectactions.SelectByValueFromBy;
import net.serenitybdd.screenplay.actions.selectactions.SelectByVisibleTextFromBy;
import net.serenitybdd.screenplay.actions.selectactions.SelectByVisibleTextFromElement;
import net.serenitybdd.screenplay.actions.selectactions.SelectByVisibleTextFromTarget;
import net.serenitybdd.screenplay.waits.WaitUntil;

public class Registra implements Task{

	private  List<List<String>> form;
	
	
	public Registra(List<List<String>> form) {
		super();
		this.form = form;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Enter.theValue(form.get(1).get(0).trim()).into(AutomationFormularioPage.FIRST_NAME));
		actor.attemptsTo(Enter.theValue(form.get(1).get(1).trim()).into(AutomationFormularioPage.LAST_NAME));
		actor.attemptsTo(Enter.theValue(form.get(1).get(2).trim()).into(AutomationFormularioPage.ADDRESS));
		actor.attemptsTo(Enter.theValue(form.get(1).get(3).trim()).into(AutomationFormularioPage.EMAIL));
		actor.attemptsTo(Enter.theValue(form.get(1).get(4).trim()).into(AutomationFormularioPage.PHONE));
		//CheckBox Hobbies
		if(form.get(1).get(5).equals("Male")) {
			actor.attemptsTo(Click.on(AutomationFormularioPage.GENDER_MALE));	
		}else {
			actor.attemptsTo(Click.on(AutomationFormularioPage.GENDER_FEMALE));
		}
		if(form.get(1).get(6).equals("Cricket")) {
			actor.attemptsTo(Click.on(AutomationFormularioPage.BUTTON_CRICKET));			
		}else {
			if(form.get(1).get(6).equals("Movies")) {
				actor.attemptsTo(Click.on(AutomationFormularioPage.BUTTON_MOVIES));
			}else {
				if(form.get(1).get(6).equals("Hockey")){
					actor.attemptsTo(Click.on(AutomationFormularioPage.BUTTON_HOCKEY));
				}else {
					System.out.println("El hobbie ingresado no es una opcion valida");
				}
			}
		}
		actor.attemptsTo(Click.on(AutomationFormularioPage.SELECT_LANGUAGES));
		actor.attemptsTo(Click.on(AutomationFormularioPage.SELECT_ENGLISH));
		actor.attemptsTo(SelectFromOptions.byVisibleText(form.get(1).get(8).trim()).from(AutomationFormularioPage.SELECT_SKILL));
		actor.attemptsTo(SelectFromOptions.byVisibleText(form.get(1).get(9).trim()).from(AutomationFormularioPage.SELECT_COUNTRY));
		actor.attemptsTo(Click.on(AutomationFormularioPage.SELECT_SELECTCOUNTRY));
		actor.attemptsTo(Enter.theValue(form.get(1).get(10).trim()).into(AutomationFormularioPage.SELECT_RESULTCOUNTRY));
		actor.attemptsTo(Hit.the(Keys.ENTER).into(AutomationFormularioPage.SELECT_RESULTCOUNTRY));
		actor.attemptsTo(SelectFromOptions.byVisibleText(form.get(1).get(11).trim()).from(AutomationFormularioPage.SELECT_YEAR));
		actor.attemptsTo(SelectFromOptions.byVisibleText(form.get(1).get(12).trim()).from(AutomationFormularioPage.SELECT_MONTH));
		actor.attemptsTo(SelectFromOptions.byVisibleText(form.get(1).get(13).trim()).from(AutomationFormularioPage.SELECT_DAY));
		actor.attemptsTo(Enter.theValue(form.get(1).get(14).trim()).into(AutomationFormularioPage.PASSWORD));
		actor.attemptsTo(Enter.theValue(form.get(1).get(15).trim()).into(AutomationFormularioPage.CONFIRM_PASSWORD));
		if(form.get(1).get(16).equals("Submit")) {
			actor.attemptsTo(Click.on(AutomationFormularioPage.BUTTON_SUBMIT));
		}else {
			actor.attemptsTo(Click.on(AutomationFormularioPage.BUTTON_REFRESH));
		}
	}

	public static Registra EnelFormulario(List<List<String>> form) {
		return Tasks.instrumented(Registra.class, form);
	}
	
	

}
