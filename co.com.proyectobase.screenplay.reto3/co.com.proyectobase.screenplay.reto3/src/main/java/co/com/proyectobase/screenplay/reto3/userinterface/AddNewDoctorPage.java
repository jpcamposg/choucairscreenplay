package co.com.proyectobase.screenplay.reto3.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class AddNewDoctorPage {
	
	
	public static final Target NAME = Target.the("Nombre del Doctor")
			.located(By.id("name"));
	public static final Target LAST_NAME = Target.the("Apellido del Doctor")
			.located(By.id("last_name"));
	public static final Target TELEPHONE = Target.the("Telefono del Doctor")
			.located(By.id("telephone"));
	public static final Target ID_TYPE = Target.the("Tipo de identificacion")
			.located(By.id("identification_type"));
	public static final Target IDENTIFICATION = Target.the("Identificacion")
			.located(By.id("identification"));
	public static final Target SAVE = Target.the("Identificacion")
			.located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[3]/div/a"));	
	public static final Target RESULT = Target.the("Identificacion")
			.located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[2]/div[2]/p"));
}
