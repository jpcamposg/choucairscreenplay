package co.com.proyectobase.screenplay.reto3.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://automatizacion.herokuapp.com/pperez/")
public class MedicalAssistancePage extends PageObject {
	
	public static final Target BTN_ADDDOCTOR = Target.the("Acceder a la pagina de agregar doctor")
			.located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[2]/div/div/div/div/div[1]/div/a[1]"));
	public static final Target ADD_PATIENT = Target.the("Agregar paciente")
			.located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[2]/div/div/div/div/div[1]/div/a[2]"));

}
