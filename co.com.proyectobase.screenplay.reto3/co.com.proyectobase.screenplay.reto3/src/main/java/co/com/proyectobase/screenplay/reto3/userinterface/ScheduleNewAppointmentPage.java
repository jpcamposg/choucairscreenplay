package co.com.proyectobase.screenplay.reto3.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class ScheduleNewAppointmentPage{

	public static final Target SCHEDULE_APPOINTMENT = Target.the("Agendar Cita")
			.located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[2]/div/div/div/div/div[1]/div/a[6]"));
	public static final Target DAY_APPOINTMENT = Target.the("Dia de la Cita")
			.located(By.id("datepicker"));
	public static final Target ID_PATIENT = Target.the("Dia de la Cita")
			.located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[3]/div/div[2]/input"));
	public static final Target ID_DOCTOR = Target.the("Dia de la Cita")
			.located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[3]/div/div[3]/input"));
	public static final Target OBSERVATIONS = Target.the("Dia de la Cita")
			.located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[3]/div/div[4]/textarea"));	
	public static final Target SAVE = Target.the("Dia de la Cita")
			.located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[3]/div/a"));
}
