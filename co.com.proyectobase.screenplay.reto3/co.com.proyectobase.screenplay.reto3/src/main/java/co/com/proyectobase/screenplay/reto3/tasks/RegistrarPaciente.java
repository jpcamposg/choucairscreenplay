package co.com.proyectobase.screenplay.reto3.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.reto3.userinterface.AddNewPatientPage;
import co.com.proyectobase.screenplay.reto3.userinterface.MedicalAssistancePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class RegistrarPaciente implements Task{

	private List<List<String>> addPatient;

	public RegistrarPaciente(List<List<String>> addPatient) {
		super();
		this.addPatient = addPatient;
	}
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Click.on(MedicalAssistancePage.ADD_PATIENT));
		actor.attemptsTo(Enter.theValue(addPatient.get(1).get(0).trim()).into(AddNewPatientPage.NAME_PATIENT));
		actor.attemptsTo(Enter.theValue(addPatient.get(1).get(1).trim()).into(AddNewPatientPage.LASTNAME_PATIENT));
		actor.attemptsTo(Enter.theValue(addPatient.get(1).get(2).trim()).into(AddNewPatientPage.TELEPHONE_PATIENT));
		actor.attemptsTo(SelectFromOptions.byVisibleText(addPatient.get(1).get(3).trim()).from(AddNewPatientPage.ID_TYPE_PATIENT));
		actor.attemptsTo(Enter.theValue(addPatient.get(1).get(4).trim()).into(AddNewPatientPage.ID_PATIENT));
		actor.attemptsTo(Click.on(AddNewPatientPage.PREPAID_HEALTH));
		actor.attemptsTo(Click.on(AddNewPatientPage.SAVE));
		
	}
	
	public static RegistrarPaciente EnelFormularioPaciente(List<List<String>> addPatient) {
		return Tasks.instrumented(RegistrarPaciente.class, addPatient);
	}

}
