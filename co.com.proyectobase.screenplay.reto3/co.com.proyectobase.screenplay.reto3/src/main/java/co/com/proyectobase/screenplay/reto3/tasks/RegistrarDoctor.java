package co.com.proyectobase.screenplay.reto3.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.reto3.userinterface.AddNewDoctorPage;
import co.com.proyectobase.screenplay.reto3.userinterface.MedicalAssistancePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class RegistrarDoctor implements Task {
	
	private List<List<String>> form;

	public RegistrarDoctor(List<List<String>> form) {
		super();
		this.form = form;
	}
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Click.on(MedicalAssistancePage.BTN_ADDDOCTOR));
		actor.attemptsTo(Enter.theValue(form.get(1).get(0).trim()).into(AddNewDoctorPage.NAME));
		actor.attemptsTo(Enter.theValue(form.get(1).get(1).trim()).into(AddNewDoctorPage.LAST_NAME));
		actor.attemptsTo(Enter.theValue(form.get(1).get(2).trim()).into(AddNewDoctorPage.TELEPHONE));
		actor.attemptsTo(SelectFromOptions.byVisibleText(form.get(1).get(3).trim()).from(AddNewDoctorPage.ID_TYPE));
		actor.attemptsTo(Enter.theValue(form.get(1).get(4).trim()).into(AddNewDoctorPage.IDENTIFICATION));
		actor.attemptsTo(Click.on(AddNewDoctorPage.SAVE));
	}
	
	public static RegistrarDoctor EnelFormulario(List<List<String>> form) {
		return Tasks.instrumented(RegistrarDoctor.class, form);
	}

}
