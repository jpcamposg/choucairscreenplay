package co.com.proyectobase.screenplay.reto3.tasks;

import co.com.proyectobase.screenplay.reto3.userinterface.MedicalAssistancePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;

public class Abrir implements Task{

	MedicalAssistancePage medicalAssistancePage;
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(medicalAssistancePage));
		
	}
	
	public static Abrir MedicalAssistancePage() {
		return Tasks.instrumented(Abrir.class);
	}

}
