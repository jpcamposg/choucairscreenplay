package co.com.proyectobase.screenplay.reto3.questions;

import co.com.proyectobase.screenplay.reto3.userinterface.AddNewDoctorPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class ResultAddDoctor implements Question<String> {

	
	public static ResultAddDoctor es() {
		return new ResultAddDoctor();
	}
	
	@Override
	public String answeredBy(Actor actor) {
		return Text.of(AddNewDoctorPage.RESULT).viewedBy(actor).asString();
	}
	
	

}
