package co.com.proyectobase.screenplay.reto3.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class AddNewPatientPage {

	
	public static final Target NAME_PATIENT = Target.the("Nombre del Paciente")
			.located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[3]/div/div[1]/input"));
	public static final Target LASTNAME_PATIENT = Target.the("Apellido del Paciente")
			.located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[3]/div/div[2]/input"));
	public static final Target TELEPHONE_PATIENT = Target.the("Telefono del Paciente")
			.located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[3]/div/div[3]/input"));	
	public static final Target ID_TYPE_PATIENT = Target.the("Tipo identificacion paciente")
			.located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[3]/div/div[4]/select"));
	public static final Target ID_PATIENT = Target.the("Identificacion Paciente")
			.located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[3]/div/div[5]/input"));
	public static final Target PREPAID_HEALTH = Target.the("Medicina Prepagada")
			.located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[3]/div/div[6]/label/input"));	
	public static final Target SAVE = Target.the("Guardar Paciente")
			.located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[3]/div/a"));
}
