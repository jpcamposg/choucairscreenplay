package co.com.proyectobase.screenplay.reto3.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.reto3.userinterface.ScheduleNewAppointmentPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class AgendarCita implements Task {

	private List<List<String>> schedule;

	public AgendarCita(List<List<String>> schedule) {
		super();
		this.schedule = schedule;
	}
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Click.on(ScheduleNewAppointmentPage.SCHEDULE_APPOINTMENT));
		actor.attemptsTo(Enter.theValue(schedule.get(1).get(0).trim()).into(ScheduleNewAppointmentPage.DAY_APPOINTMENT));
		actor.attemptsTo(Enter.theValue(schedule.get(1).get(1).trim()).into(ScheduleNewAppointmentPage.ID_PATIENT));
		actor.attemptsTo(Enter.theValue(schedule.get(1).get(2).trim()).into(ScheduleNewAppointmentPage.ID_DOCTOR));
		actor.attemptsTo(Enter.theValue(schedule.get(1).get(3).trim()).into(ScheduleNewAppointmentPage.OBSERVATIONS));
		actor.attemptsTo(Click.on(ScheduleNewAppointmentPage.SAVE));
		
	}
	
	public static AgendarCita EnelFormularioCita(List<List<String>> schedule) {
		return Tasks.instrumented(AgendarCita.class, schedule);
	}

}
