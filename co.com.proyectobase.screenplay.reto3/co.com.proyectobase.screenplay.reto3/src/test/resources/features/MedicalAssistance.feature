#Author: your.email@your.domain.com
@tag
Feature: Manage medical appointment
  How patient
  I want to perform the request of a medical appointment

  @tag1
  Scenario: Perform a doctors registration 
    Given that Carlos needs to registry a new doctor
    When He registers in the Hospital Administration Aplication
    		|<name>	|<last_name>	|<telephone>|<idtype>							|<identification>	|
    		|Jose		|zapata				|3112509877	|Cédula de ciudadanía	|94391167					|
    Then He verifies that the message Data saved correctly appears on the screen
    		|<result>											|
    		|Datos guardados correctamente.|
    		
  @tag2
  Scenario: Perform a patient registration 
    Given that Carlos needs to registry a new patient
    When He registers a new patient in the Hospital Administration Aplication
    		|<name>	|<last_name>	|<telephone>|<idtype>							|<identification>	|
    		|Juan		|Betancurt		|3112519877	|Cédula de ciudadanía	|93278981					|
    Then He verifies that the message Data saved appears
    		|<result>											|
    		|Datos guardados correctamente.|

  @tag3
  Scenario Outline: Make the Schedule of an Appointment
  	Given That Carlos needs to attend to the Doctor
  	When He performs the scheduling of an Appointment
  	|<dayappointment>	|<idpatient>			|<iddoctor>				|<observations>					|
  	|28/09/2018				|93278981					|94391167					|Cita Medica Ortopedista|
  	Then He verifies that the message Data saved correctly appears
  		|<resultcita>										|
  		|Datos guardados correctamente.	|

    Examples: 
      | name  | value | status  |
      | name1 |     5 | success |
      | name2 |     7 | Fail    |
