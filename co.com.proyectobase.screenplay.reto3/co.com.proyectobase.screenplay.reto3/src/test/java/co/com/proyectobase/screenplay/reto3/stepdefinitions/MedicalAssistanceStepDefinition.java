package co.com.proyectobase.screenplay.reto3.stepdefinitions;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.List;

import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.reto3.questions.ResultAddDoctor;
import co.com.proyectobase.screenplay.reto3.tasks.Abrir;
import co.com.proyectobase.screenplay.reto3.tasks.AgendarCita;
import co.com.proyectobase.screenplay.reto3.tasks.RegistrarDoctor;
import co.com.proyectobase.screenplay.reto3.tasks.RegistrarPaciente;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class MedicalAssistanceStepDefinition {
	
	@Managed(driver ="chrome")
	private WebDriver hisBrowser;
	private Actor carlos = Actor.named("carlos");
	
	@Before
	public void configuracionInicial() {
		carlos.can(BrowseTheWeb.with(hisBrowser));
	}
	
	@Given("^that Carlos needs to registry a new doctor$")
	public void thatCarlosNeedsToRegistryANewDoctor() throws Exception {
	    carlos.wasAbleTo(Abrir.MedicalAssistancePage());
	}


	@When("^He registers in the Hospital Administration Aplication$")
	public void heRegistersInTheHospitalAdministrationAplication(DataTable form) throws Exception {
		List<List<String>> data = form.raw();
		carlos.attemptsTo(RegistrarDoctor.EnelFormulario(data));
	}

	@Then("^He verifies that the message Data saved correctly appears on the screen$")
	public void heVerifiesThatTheMessageDataSavedCorrectlyAppearsOnTheScreen(DataTable Respuesta) throws Exception {
		List<List<String>> dataresult = Respuesta.raw();
		carlos.should(seeThat(ResultAddDoctor.es(), equalTo(dataresult.get(1).get(0).trim())));
		
	}
	
	//Registrar un nuevo Paicente//
	
	@Given("^that Carlos needs to registry a new patient$")
	public void thatCarlosNeedsToRegistryANewPatient() throws Exception {
	    carlos.wasAbleTo(Abrir.MedicalAssistancePage());
	}


	@When("^He registers a new patient in the Hospital Administration Aplication$")
	public void heRegistersANewPatientInTheHospitalAdministrationAplication(DataTable AddPatient) throws Exception {
		List<List<String>> dataPatient = AddPatient.raw();
		carlos.attemptsTo(RegistrarPaciente.EnelFormularioPaciente(dataPatient));
	}

	@Then("^He verifies that the message Data saved appears$")
	public void heVerifiesThatTheMessageDataSavedAppears(DataTable ResultAdd) throws Exception {
		List<List<String>> resultAdd = ResultAdd.raw();
		carlos.should(seeThat(ResultAddDoctor.es(), equalTo(resultAdd.get(1).get(0).trim())));
		
	}
	
	// Registrar una agendamiento de cita Medica//
	
	@Given("^That Carlos needs to attend to the Doctor$")
	public void thatCarlosNeedsToAttendToTheDoctor() throws Exception {
		carlos.wasAbleTo(Abrir.MedicalAssistancePage());
	}


	@When("^He performs the scheduling of an Appointment$")
	public void hePerformsTheSchedulingOfAnAppointment(DataTable Schedule) throws Exception {
		List<List<String>> schedule = Schedule.raw();
		carlos.attemptsTo(AgendarCita.EnelFormularioCita(schedule));
	}
	
	@Then("^He verifies that the message Data saved correctly appears$")
	public void heVerifiesThatTheMessageDataSavedCorrectlyAppears(DataTable RespuestaCita) throws Exception {
		List<List<String>> dataresultcita = RespuestaCita.raw();
		carlos.should(seeThat(ResultAddDoctor.es(), equalTo(dataresultcita.get(1).get(0).trim())));
		
	}

}
