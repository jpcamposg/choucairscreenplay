package co.com.proyectobase.screenplay.reto3.runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		features="src/test/resources/features/MedicalAssistance.feature",
		tags= "@tag1,@tag2,@tag3",
		glue="co.com.proyectobase.screenplay.reto3.stepdefinitions",
		snippets=SnippetType.CAMELCASE		)
public class RunnerTags {

}
