package co.com.proyectobase.screenplay.reto4.stepdefinitions;

import java.util.List;

import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.reto4.model.MaxTimeModel;
import co.com.proyectobase.screenplay.reto4.tasks.Abrir;
import co.com.proyectobase.screenplay.reto4.tasks.RegistrarDia;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;


public class AutomatioMaxtimeStepDefinition {

	@Managed(driver ="chrome")
	private WebDriver hisBrowser;
	private Actor juan = Actor.named("juan");
	
	@Before
	public void configuracionInicial() {
		juan.can(BrowseTheWeb.with(hisBrowser));
	}
	
	@Given("^How Test Analyst$")
	public void howTestAnalyst() throws Exception {
	    juan.wasAbleTo(Abrir.Maxtime());
	}


	@When("^I want to perform the report of my activities in MaxTime$")
	public void iWantToPerformTheReportOfMyActivitiesInMaxTime(List<MaxTimeModel> maxTimeModel){
		juan.attemptsTo(RegistrarDia.EnelFormulario(maxTimeModel));
	}

	@Then("^To fulfill the task that I must carry out daily$")
	public void toFulfillTheTaskThatIMustCarryOutDaily(){
		
	}
}