package co.com.proyectobase.screenplay.reto4.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.choucairtesting.com/MaxTimeCHC/Login.aspx?")
public class MaxtimePage extends PageObject {
	
	public static final Target USUARIO = Target.the("Usuario")
			.locatedBy("//td/input[contains (@id, 'UserName_Edit')]");
	public static final Target CONTRASEÑA = Target.the("Contraseña")
			.locatedBy("//td/input[contains (@id, 'Password_Edit')]");
	public static final Target BTN_LOGIN = Target.the("Acceder a Maxtime")
			.locatedBy("//a[@title='Conectarse']");

}