package co.com.proyectobase.screenplay.reto4.tasks;

import co.com.proyectobase.screenplay.reto4.userinterface.MaxtimePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class Abrir implements Task{

	MaxtimePage maxtimePage;
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(maxtimePage));
		
	}
	
	public static Abrir Maxtime() {
		return Tasks.instrumented(Abrir.class);
	}

}