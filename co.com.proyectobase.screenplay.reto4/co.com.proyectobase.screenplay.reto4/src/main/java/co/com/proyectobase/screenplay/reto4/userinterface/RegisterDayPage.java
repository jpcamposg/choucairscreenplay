package co.com.proyectobase.screenplay.reto4.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class RegisterDayPage extends PageObject {
	
	
	public static final Target BTN_FILTRAR = Target.the("Boton para filtrar Usuario")
			.located(By.xpath("//*[@id=\"Vertical_v1_LE_v2_col1\"]/table/tbody/tr/td[2]/img[2]"));
	public static final Target USUARIO = Target.the("Filtrar por Usuario")
			.located(By.id("Vertical_v1_LE_v2_HFListBox_LBT"));
	public static final Target REPORTE_TIEMPO = Target.the("Filtrar por Usuario")
			.locatedBy("//*[@id=\"Vertical_NC_NB_I0i0_\"]");
	public static final Target DIA = Target.the("Seleccionar Dia")		
			.locatedBy("//*[@id='Vertical_UPVSC']//table[contains(@id,'_Fecha')]//span[contains(@id, 'Fecha_View')]");	
	public static final Target AGREGAR_TAREA = Target.the("Crear Nueva Tarea")
			.locatedBy("//li/div[contains(@id, 'ReporteDetallado_ToolBar_Menu_DXI0_T')]");	
	public static final Target AGREGAR_PROYECTO = Target.the("Agregar Proyecto")
			.locatedBy("//*[contains(@id, 'Proyecto_Edit_Find_B' )]//div[2][contains(@id, 'Proyecto_Edit_Find_CD')]");
	public static final Target BUSCAR_CODCLIENTE = Target.the("Buscar codigo del cliente a facturar")
			.locatedBy("//tbody/tr/td/input[contains(@id, 'SearchActionContainer_Menu_ITCNT0_xaf_a0_Ed_I')]");	
	public static final Target CODIGO_CLIENTE = Target.the("Codigo del cliente a facturar")
			.locatedBy("//tr/td[contains(@id, 'Dialog_v8_LE_v9_tccell0_4')]");	
	public static final Target TIPO_HORA = Target.the("Tipo de hora laboral")
	.located(By.xpath("//tr/td/input[contains(@id, 'TipoHora_Edit_DD_I')]"));
	public static final Target SERVICIO = Target.the("Tipo de servicio")
	.located(By.xpath("//td/div[2][contains(@id, 'Servicio_Edit_Find_CD')]"));	
	public static final Target BUSCAR_SERVICIO = Target.the("Buscar tipo de servicio")
	.located(By.xpath("//tr/td/input[contains(@id, 'SearchActionContainer_Menu_ITCNT0_xaf_a0_Ed_I')]"));	
	public static final Target TIPO_SERVICIO = Target.the("Tipo de servicio")
	.located(By.xpath("//tr/td/span[contains(@id, 'cell1_0_xaf_Nombre_View')]"));
	public static final Target ACTIVIDAD = Target.the("Actidad")
	.located(By.xpath("//tr/td[2]/input[contains(@id, 'Actividad_Edit_DD_I')]"));
	public static final Target CAUSAL_OCIO = Target.the("Causal del ocio")
	.located(By.xpath("//tr/td[2]/input[contains(@id, 'CausalOcioso_Edit_DD_I')]"));	
	public static final Target HORAS_EJECUTADAS = Target.the("Horar trabajadas")
	.located(By.xpath("//tr/td/input[contains(@id, 'Horas_Edit_I')]"));	
	public static final Target HORAS_ADICIONALES = Target.the("Horas adicionales a pagar")
	.located(By.xpath("//tr/td/input[contains(@id, 'HorasAdicionalesAPagar_Edit_I')]"));	
	public static final Target HORAS_COMPENSATORIO = Target.the("Horas de compensario")
	.located(By.xpath("//tr/td/input[contains(@id, 'HorasAdicionalesCompensatorio_Edit_I')]"));	
	public static final Target COMENTARIO = Target.the("Comentarios")
	.located(By.xpath("//tr/td/input[contains(@id, 'Comentario_Edit_I')]"));	
	public static final Target GUARDAR = Target.the("Guardar Tarea")
	.located(By.xpath("//li/div[contains(@id, 'Vertical_EditModeActions2_Menu_DXI1_T')]"));	
	public static final Target CERRAR_DIA = Target.the("Cerrar dia")
	.located(By.xpath("//li/div[contains(@id, 'Vertical_TB_Menu_DXI1_T')]"));	
	public static final Target ACEPTAR = Target.the("Aceptar cierre")
	.located(By.xpath("//li/div[contains(@id, 'Dialog_actionContainerHolder_Menu_DXI0_T')]"));
}
