package co.com.proyectobase.screenplay.reto4.tasks;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

import java.util.List;

import org.openqa.selenium.Keys;

import co.com.proyectobase.screenplay.reto4.userinterface.MaxtimePage;
import co.com.proyectobase.screenplay.reto4.userinterface.RegisterDayPage;
import co.com.proyectobase.screenplay.reto4.model.MaxTimeModel;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Hit;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.waits.WaitUntil;

public class RegistrarDia implements Task {

	private List<MaxTimeModel> maxTimeModel;


	public RegistrarDia(List<MaxTimeModel> maxTimeModel) {
		super();
		this.maxTimeModel = maxTimeModel;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
			
		 try {
			 actor.attemptsTo(Enter.theValue(maxTimeModel.get(0).getUsuario()).into(MaxtimePage.USUARIO));
			 actor.attemptsTo(Enter.theValue(maxTimeModel.get(0).getContraseña()).into(MaxtimePage.CONTRASEÑA));
			 actor.attemptsTo(Click.on(MaxtimePage.BTN_LOGIN), WaitUntil.the(RegisterDayPage.DIA,
						isVisible()).forNoMoreThan(15).seconds());
			Thread.sleep(20000);
			if(maxTimeModel.get(0).getIsLaboralday().equals("Yes")) {					 
				 actor.attemptsTo(Click.on(RegisterDayPage.DIA), WaitUntil.the(RegisterDayPage.AGREGAR_TAREA,
						 isVisible()).forNoMoreThan(15).seconds());	
				 actor.attemptsTo(Click.on(RegisterDayPage.AGREGAR_TAREA));
				 Thread.sleep(20000);
				 actor.attemptsTo(Click.on(RegisterDayPage.AGREGAR_PROYECTO), WaitUntil.the(RegisterDayPage.BUSCAR_CODCLIENTE,
						 isVisible()).forNoMoreThan(15).seconds());
				 Thread.sleep(20000);
				 actor.attemptsTo(Enter.theValue(maxTimeModel.get(0).getCodCliente()).into(RegisterDayPage.BUSCAR_CODCLIENTE));
				 actor.attemptsTo(Hit.the(Keys.ENTER).into(RegisterDayPage.BUSCAR_CODCLIENTE), WaitUntil.the(RegisterDayPage.CODIGO_CLIENTE,
						 isVisible()).forNoMoreThan(15).seconds());
				 actor.attemptsTo(Click.on(RegisterDayPage.CODIGO_CLIENTE));
				 actor.attemptsTo(Enter.theValue(maxTimeModel.get(0).getTipoHora()).into(RegisterDayPage.TIPO_HORA));
				 actor.attemptsTo(Click.on(RegisterDayPage.SERVICIO));
				 actor.attemptsTo(Enter.theValue(maxTimeModel.get(0).getServicio()).into(RegisterDayPage.BUSCAR_SERVICIO));
				 actor.attemptsTo(Hit.the(Keys.ENTER).into(RegisterDayPage.BUSCAR_SERVICIO));
				 actor.attemptsTo(Enter.theValue(maxTimeModel.get(0).getActividad()).into(RegisterDayPage.ACTIVIDAD));
				 actor.attemptsTo(Enter.theValue(maxTimeModel.get(0).getCausalOcio()).into(RegisterDayPage.CAUSAL_OCIO));
				 actor.attemptsTo(Enter.theValue(maxTimeModel.get(0).gethEjecutadas()).into(RegisterDayPage.HORAS_EJECUTADAS));
				 actor.attemptsTo(Enter.theValue(maxTimeModel.get(0).gethAdicionales()).into(RegisterDayPage.HORAS_ADICIONALES));
				 actor.attemptsTo(Enter.theValue(maxTimeModel.get(0).gethCompensatorio()).into(RegisterDayPage.HORAS_COMPENSATORIO));
				 actor.attemptsTo(Enter.theValue(maxTimeModel.get(0).getComentarios()).into(RegisterDayPage.COMENTARIO));
				 actor.attemptsTo(Click.on(RegisterDayPage.GUARDAR));
				 actor.attemptsTo(Click.on(RegisterDayPage.CERRAR_DIA));
				 actor.attemptsTo(Click.on(RegisterDayPage.ACEPTAR));
			}else {
				actor.attemptsTo(Click.on(RegisterDayPage.CERRAR_DIA));
			    actor.attemptsTo(Click.on(RegisterDayPage.ACEPTAR));
			}
			
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		 
		 
		
	}
	
	public static RegistrarDia EnelFormulario(List<MaxTimeModel> maxTimeModel) {
		return Tasks.instrumented(RegistrarDia.class, maxTimeModel);
	}

}
