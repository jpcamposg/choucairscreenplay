package co.com.proyectobase.screenplay.reto4.model;

public class MaxTimeModel {

	private String usuario;
	private String contraseña;
	private String isLaboralday;
	private String codCliente;
	private String tipoHora;
	private String servicio;
	private String actividad;
	private String causalOcio;
	private String hEjecutadas;
	private String hAdicionales;
	private String hCompensatorio;
	private String comentarios;
	
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getContraseña() {
		return contraseña;
	}
	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}
	public String getIsLaboralday() {
		return isLaboralday;
	}
	public void setIsLaboralday(String isLaboralday) {
		this.isLaboralday = isLaboralday;
	}
	public String getCodCliente() {
		return codCliente;
	}
	public void setCodCliente(String codCliente) {
		this.codCliente = codCliente;
	}
	public String getTipoHora() {
		return tipoHora;
	}
	public void setTipoHora(String tipoHora) {
		this.tipoHora = tipoHora;
	}
	public String getServicio() {
		return servicio;
	}
	public void setServicio(String servicio) {
		this.servicio = servicio;
	}
	public String getActividad() {
		return actividad;
	}
	public void setActividad(String actividad) {
		this.actividad = actividad;
	}
	public String getCausalOcio() {
		return causalOcio;
	}
	public void setCausalOcio(String causalOcio) {
		this.causalOcio = causalOcio;
	}
	public String gethEjecutadas() {
		return hEjecutadas;
	}
	public void sethEjecutadas(String hEjecutadas) {
		this.hEjecutadas = hEjecutadas;
	}
	public String gethAdicionales() {
		return hAdicionales;
	}
	public void sethAdicionales(String hAdicionales) {
		this.hAdicionales = hAdicionales;
	}
	public String gethCompensatorio() {
		return hCompensatorio;
	}
	public void sethCompensatorio(String hCompensatorio) {
		this.hCompensatorio = hCompensatorio;
	}
	public String getComentarios() {
		return comentarios;
	}
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}
	
	
	
	
}
